﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DDosAttackSim
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());

        }
    }

    /// <summary>
    /// User Class: Stores to information about user which recieved from XML file.
    /// </summary>
    public class User
    {
        public string ip;
        public int request_amount;
        public bool is_banned;

        public TimeSpan first_request_time;
        public User(string ip = "Ip not set!", int request_amount = 0, bool is_banned = false)
        {
            this.ip = ip;
            this.request_amount = request_amount;
            this.is_banned = is_banned;
        }

    }

    /// <summary>
    /// Detector Class: Tries to detect attack with checking each XML files attacker or not.
    /// Includes NFA
    /// a Singleton Class
    /// </summary>
    public class Detector
    {
        private Detector() { }

        private static Detector instance = null;
        public static Detector Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Detector();
                }
                return instance;
            }
        }
        private string ip;
        public string IP
        {
            get { return this.ip; }
            set {  this.ip = value; }
        }
        enum State { DoesUserExist, BanCheck, AddUser, IncrementRequest, CheckForAttack,
            BanUser, DeadStateNoAttack, DeadStateUserWasBanned, FinalStateAttackerBanned  };
        State state = State.DoesUserExist;
        bool flag = false;
       
        UserDatabaseAPI api = UserDatabaseAPI.Instance;
        
        /// <summary>
        /// NFA
        /// </summary>
        public void DetectAttack()
        {
            state = State.DoesUserExist;
            flag = false;
            while (true)
            {

                switch (state)
                {
                    case State.DoesUserExist:
                        if (api.userExist(ip) == 1)
                        {
                            state = State.BanCheck;
                        }
                        else if (api.userExist(ip) == 0)
                        {
                            state = State.AddUser;
                        }
                        break;

                    case State.BanCheck:
                        if (api.isUserBanned(ip) == 1)
                        {
                            state = State.DeadStateUserWasBanned;
                        }
                        else if (api.isUserBanned(ip) == 0)
                        {
                            state = State.IncrementRequest;
                        }
                        break;

                    case State.AddUser:
                        if (true)
                        {
                            api.addUser(ip);
                            state = State.IncrementRequest;
                        }
                        break;

                    case State.IncrementRequest:
                        if (true)
                        {
                            api.incUserReqCounter(ip);
                            state = State.CheckForAttack;
                        }
                        break;

                    case State.CheckForAttack:
                        if (api.isUserSus(ip) == 1)
                        {
                            state = State.BanUser;
                        }
                        else if (api.isUserSus(ip) == 0)
                        {
                            state = State.DeadStateNoAttack;
                        }
                        break;

                    case State.BanUser:
                        if (true)
                        {
                            api.banUser(ip);
                            state = State.FinalStateAttackerBanned;
                        }
                        break;

                    case State.DeadStateNoAttack:
                        flag = true;
                        //dead state
                        break;

                    case State.DeadStateUserWasBanned:
                        flag = true;
                        //dead state
                        break;

                    case State.FinalStateAttackerBanned:
                        flag = true;
                        //accepted state user was banned
                        break;

                }
                if (flag)
                {
                    break;
                }
            }
                

        }


    }


    /// <summary>
    /// UserDatabaseAPI Class: Stores Users information in Array List.
    /// Brings informations about user status.
    /// a Singleton Class
    /// </summary>
    public class UserDatabaseAPI
    {
        private UserDatabaseAPI() { }

        private static UserDatabaseAPI instance = null;
        public static UserDatabaseAPI Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new UserDatabaseAPI();
                }
                return instance;
            }
        }
        TimeSpan start_time;
        TimeSpan current_time;
        TimeSpan sus_time = new TimeSpan(0, 0, 0); //to-do
        List<User> users = new List<User>();

        /// <summary>
        /// If user exists in List, returns 1.
        /// If user does not exist in List, returns 0.
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        public int userExist(string ip)
        {
            for (int i = 0; i < users.Count; i++)
            {
                if (users[i].ip == ip)
                {
                    return 1;
                }
            }
            return 0;
        }

        /// <summary>
        /// Adds user to List.
        /// </summary>
        /// <param name="ip"></param>
        public void addUser(string ip)
        {
            User newUser = new User(ip);
            this.setCurrentTime();
            newUser.first_request_time = current_time;
            users.Add(newUser);
            return;
        }


        /// <summary>
        /// Finds user in List and return this.
        /// If user does not exist, returns -1 .
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        public int findUser(string ip)
        {
            for (int i = 0; i < users.Count; i++)
            {
                if (users[i].ip == ip)
                {
                    return i;
                }
            }
            return -1;
        }

        /// <summary>
        /// Sets the start time by using System Time.
        /// </summary>
        public void startTime()
        {
            start_time = DateTime.Now.TimeOfDay;
            
        }

        /// <summary>
        /// Returns the start time.
        /// </summary>
        /// <returns></returns>
        public TimeSpan getstartTime()
        {
            return start_time;
        }

        /// <summary>
        /// Sets the current time by using System Time.
        /// </summary>
        public void setCurrentTime()
        {
            current_time = DateTime.Now.TimeOfDay;

        }

        /// <summary>
        /// Returns the current time.
        /// </summary>
        /// <returns></returns>
        public TimeSpan getcurrentTime()
        {
            return current_time;
        }

        /// <summary>
        /// Bans the user by using IP.
        /// </summary>
        /// <param name="ip"></param>
        public void banUser(string ip)
        {
            for (int i = 0; i < users.Count; i++)
            {
                if (users[i].ip == ip)
                {
                    users[i].is_banned = true;
                    return;
                }
            }
        }

        /// <summary>
        /// Checks the user is banned.
        /// If user is banned, returns 1 .
        /// If user is not banned, returns 0;
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        public int isUserBanned(string ip)
        {
            for (int i = 0; i < users.Count; i++)
            {
                if (users[i].ip == ip)
                {
                    if (users[i].is_banned)
                    {
                        return 1;
                    }
                }
            }
            return 0;
        }

        /// <summary>
        /// Sets the sus time.
        /// </summary>
        /// <param name="miliseconds"></param>
        public void setSusTime(int miliseconds)
        {
            TimeSpan time = new TimeSpan(0, 0,0,0,miliseconds);
            sus_time = sus_time.Add(time);

        }

        /// <summary>
        /// Checks if user has made enought requests to meet sus criteria.
        /// (Users first request time) / (Requests made) > (Suspicious time)
        /// </summary>
        /// <param name="ip">User's ip as string</param>
        /// <returns>True if user meets suspicious requests </returns>
        public int isUserSus(string ip)
        {
            for (int i = 0; i < users.Count; i++)
            {
                //to-do sus_time to int. to seconds maybe? maybe keep all time in seconds
                if (users[i].ip == ip && calcTimeDivReq(ip) >= sus_time.TotalMilliseconds )
                {
                    return 1;
                }
            }

            return 0;
        }

        /// <summary>
        /// Calculate user request frequency. Elapsed time from program start divided by user request amount.
        /// </summary>
        /// <param name="ip">User's ip as string</param>
        /// <returns></returns>
        public int calcTimeDivReq(string ip)
        {

            for (int i = 0; i < users.Count; i++)
            {
                if (users[i].ip == ip)
                {
                    //to-do how to substract time? maybe keep all time in seconds? after 00.00 pm?
                    this.setCurrentTime();
                    return (int)(current_time.Subtract(users[i].first_request_time).TotalMilliseconds / users[i].request_amount   );
                }
            }
            
            return -1;
        }

        /// <summary>
        /// Increments given user's request counter by 1.
        /// </summary>
        /// <param name="ip">User's ip as string</param>
        public void incUserReqCounter(string ip)
        {
            for (int i = 0; i < users.Count; i++)
            {
                if (users[i].ip == ip)
                {
                    users[i].request_amount++;
                    return;
                }
            }
        }

        /// <summary>
        /// Resets the sus time and Array List.
        /// </summary>
        public void Reset()
        {
            users.Clear();
            sus_time = sus_time.Subtract(sus_time);
        }
    }
}

