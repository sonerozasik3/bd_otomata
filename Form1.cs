﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Threading;


namespace DDosAttackSim
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Choose Source Folder Button :
        /// User can choose source folder to read xml files.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                FolderBrowserDialog ofd = new FolderBrowserDialog();

                string fileName = "";

                if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    fileName = ofd.SelectedPath;
                }
                lbl_filepath.Text = fileName;


                lbl_filecount.Text = Directory.GetFiles(fileName, "*.xml", SearchOption.AllDirectories).Length.ToString(); // Will Retrieve count of files XML extension in directry and sub directries
            }
            catch (Exception)
            {
                lbl_filepath.Text = "NO PATH EXIST!";
                MessageBox.Show("You have to choose a folder!", "ERROR!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Generate XML without attackers Button:
        /// This button generates dataset which includes 100 XML files without attackers. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                FolderBrowserDialog ofd = new FolderBrowserDialog();
                string fileName = "";

                if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    fileName = ofd.SelectedPath;
                }
                var rand = new Random();
                XmlWriterSettings xmlWriterSettings = new XmlWriterSettings()
                {
                    Indent = true,
                    IndentChars = "\t",
                    NewLineOnAttributes = true
                };
                for (int i = 1; i <= 100; i++)
                {
                    int ip1 = rand.Next(70, 96);
                    int ip2 = rand.Next(120, 200);
                    int ip3 = rand.Next(10, 90);
                    int ip4 = rand.Next(10, 90);
                    using (XmlWriter writer = XmlWriter.Create(fileName + "\\Test" + i + ".xml", xmlWriterSettings))
                    {
                        writer.WriteStartElement("request");
                        writer.WriteElementString("ID", (i).ToString());
                        writer.WriteElementString("IP", (ip1).ToString() + "." + (ip2).ToString() + "." + (ip3).ToString() + "." + (ip4).ToString());
                        writer.WriteElementString("Intent", "User");
                        writer.WriteElementString("Status", "Allowed");
                        writer.WriteEndElement();
                        writer.Flush();
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("You have to choose a folder!", "ERROR!",MessageBoxButtons.OK,MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Generate XML with attackers Button:
        /// This button generates dataset which includes 100 XML files with attackers.  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                FolderBrowserDialog ofd = new FolderBrowserDialog();
                string fileName = "";

                if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    fileName = ofd.SelectedPath;
                }
                var rand = new Random();
                XmlWriterSettings xmlWriterSettings = new XmlWriterSettings()
                {
                    Indent = true,
                    IndentChars = "\t",
                    NewLineOnAttributes = true
                };

                int ddos = rand.Next(3, 20);
                int ddos1 = rand.Next(30, 50);
                int ddos2 = rand.Next(60, 80);

                for (int i = 1; i <= 100; i++)
                {
                    if (ddos == i || ddos1 == i || ddos2 == i)
                    {
                        int ip1 = rand.Next(70, 96);
                        int ip2 = rand.Next(120, 200);
                        int ip3 = rand.Next(10, 90);
                        int ip4 = rand.Next(10, 90);
                        int atackersize = rand.Next(7, 15);
                        for (int j = i; j < (i + atackersize); j++)
                        {
                            using (XmlWriter writer = XmlWriter.Create(fileName + "\\Test" + j + ".xml", xmlWriterSettings))
                            {
                                writer.WriteStartElement("request");
                                writer.WriteElementString("ID", (j).ToString());
                                writer.WriteElementString("IP", (ip1).ToString() + "." + (ip2).ToString() + "." + (ip3).ToString() + "." + (ip4).ToString());
                                writer.WriteElementString("Intent", "Attacker");
                                writer.WriteElementString("Status", "Allowed");
                                writer.WriteEndElement();
                                writer.Flush();
                            }
                        }
                        i += atackersize - 1;
                    }
                    else
                    {
                        using (XmlWriter writer = XmlWriter.Create(fileName + "\\Test" + i + ".xml", xmlWriterSettings))
                        {
                            writer.WriteStartElement("request");
                            writer.WriteElementString("ID", (i).ToString());
                            writer.WriteElementString("IP", (rand.Next(70, 96)).ToString() + "." + (rand.Next(120, 200)).ToString() + "." + (rand.Next(10, 90)).ToString() + "." + (rand.Next(10, 90)).ToString());
                            writer.WriteElementString("Intent", "User");
                            writer.WriteElementString("Status", "Allowed");
                            writer.WriteEndElement();
                            writer.Flush();
                        }
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("You have to choose a folder!", "ERROR!", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
        }

        /// <summary>
        /// Start Button:
        /// This button starts the simulation to read XML files from source folder.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click_1(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            UserDatabaseAPI api = UserDatabaseAPI.Instance;
            api.Reset();
            api.startTime();
            api.setSusTime(220);

            string s1 = "", ip = "", s3 = "", s4 = "";


            for (int i = 1; i <= int.Parse(lbl_filecount.Text); i++)
            {
               
                string filename = lbl_filepath.Text + "//Test";
                filename += i.ToString() + ".xml";
                XmlTextReader xtr = new XmlTextReader(filename);

                while (xtr.Read())
                {
                    if (xtr.NodeType == XmlNodeType.Element && xtr.Name == "ID")
                    {
                        s1 = xtr.ReadElementContentAsString();
                    }
                    else if (xtr.NodeType == XmlNodeType.Element && xtr.Name == "IP")
                    {
                        ip = xtr.ReadElementContentAsString();

                    }
                    else if (xtr.NodeType == XmlNodeType.Element && xtr.Name == "Intent")
                    {
                        s3 = xtr.ReadElementContentAsString();

                    }
                    else if (xtr.NodeType == XmlNodeType.Element && xtr.Name == "Status")
                    {
                        s4 = xtr.ReadElementContentAsString();

                    }
                    
                    
                }
                // Detecting attack
                Detector detector = Detector.Instance;
                detector.IP = ip;
                detector.DetectAttack();

                if (api.isUserBanned(ip) == 1)
                    s4 = "Banned";
                else
                    s4 = "Allowed";
                string[] row = { s1, ip, s3, s4 };

                ListViewItem item = new ListViewItem(row);
                listView1.Items.Add(item);
                var rand = new Random();
                int sleep = rand.Next(200, 300);
                Thread.Sleep(sleep);
                listView1.Refresh();
                listView1.Items[listView1.Items.Count - 1].EnsureVisible();

            }
        }
        /// <summary>
        /// Help Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_help_Click(object sender, EventArgs e)
        {
            Help help = new Help();
            help.Show();
        }
    }
    
}
